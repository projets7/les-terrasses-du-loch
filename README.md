# Les Terrasses du Loch

* This is the lodge presentation website. Located in Auray, France (Bretagne).
* Simple HTML, CSS and JS
* Translation available in French, English and German.
* Available on [lesterrassesduloch.fr](https://www.lesterrassesduloch.fr).

## French keywords
Gite, Auray, Gite de France, Guidel, Rivière, Rivière d'Auray, Confortable, Charme, Paisible, Reposant, Déco, Bateau, Mer, Morbihan

---
*Theme based on Astral by HTML5 UP* | Enjoy :rocket:
