var langs = ['fr', 'en', 'de'];
var langCode = 'fr';
var langJS = null;

var translate = function (jsdata){
	$("[tkey]").each (function (index)
	{
		var strTr = jsdata [$(this).attr ('tkey')];
	    $(this).html (strTr);
	});
}

var translate_to = function(langCode){
	$.getJSON('assets/js/lang/'+langCode+'.json', translate);
}

langCode = navigator.language.substr (0, 2);

if (langCode in langs)
	$.getJSON('assets/js/lang/'+langCode+'.json', translate);
